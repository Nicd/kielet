import gleeunit
import gleeunit/should
import kielet.{gettext as g_, ngettext as n_}
import kielet/context
import kielet/database
import kielet/language
import simplifile

pub fn main() {
  gleeunit.main()
}

pub fn noop_singular_test() {
  let context = context.Context(database: database.new(), language: "en")
  should.equal(g_(context, "Hello, world!"), "Hello, world!")
}

pub fn noop_plural_singular_test() {
  let context = context.Context(database: database.new(), language: "en")
  should.equal(n_(context, "%s person", "%s people", 1), "%s person")
}

pub fn noop_plural_plural_test() {
  let context = context.Context(database: database.new(), language: "en")
  should.equal(n_(context, "%s person", "%s people", 2), "%s people")
}

pub fn wrong_language_test() {
  let ctx = load_languages()
  should.equal(
    g_(
      ctx,
      "Gleam is a friendly language for building type-safe systems that scale!",
    ),
    "Gleam is a friendly language for building type-safe systems that scale!",
  )
}

pub fn fi_singular_test() {
  let ctx = load_languages()
  should.equal(
    g_(
      context.Context(..ctx, language: "fi"),
      "Gleam is a friendly language for building type-safe systems that scale!",
    ),
    "Gleam on ystävällinen kieli skaalautuvien ja tyyppiturvallisten järjestelmien rakentamiseen!",
  )
}

pub fn fi_plural_test() {
  let ctx = load_languages()
  let ctx = context.Context(..ctx, language: "fi")
  should.equal(
    n_(ctx, "Oh look, it's a duck", "Oh look, it's %s ducks", 1),
    "Oho kato, ankka",
  )

  should.equal(
    n_(ctx, "Oh look, it's a duck", "Oh look, it's %s ducks", 2),
    "Oho kato, %s ankkaa",
  )
}

pub fn uk_plural_test() {
  let ctx = load_languages()
  let ctx = context.Context(..ctx, language: "uk")
  should.equal(
    n_(ctx, "Oh look, it's a duck", "Oh look, it's %s ducks", 1),
    "там %s качка",
  )

  should.equal(
    n_(ctx, "Oh look, it's a duck", "Oh look, it's %s ducks", 2),
    "там %s качки",
  )

  should.equal(
    n_(ctx, "Oh look, it's a duck", "Oh look, it's %s ducks", 11),
    "там %s качок",
  )

  should.equal(
    n_(ctx, "Oh look, it's a duck", "Oh look, it's %s ducks", 12),
    "там %s качок",
  )

  should.equal(
    n_(ctx, "Oh look, it's a duck", "Oh look, it's %s ducks", 5),
    "там %s качок",
  )

  should.equal(
    n_(ctx, "Oh look, it's a duck", "Oh look, it's %s ducks", 21),
    "там %s качка",
  )

  should.equal(
    n_(ctx, "Oh look, it's a duck", "Oh look, it's %s ducks", 22),
    "там %s качки",
  )

  should.equal(
    n_(ctx, "Oh look, it's a duck", "Oh look, it's %s ducks", 25),
    "там %s качок",
  )
}

pub fn no_plural_forms_test() {
  let mo_file = "./test/locale/no-plural-forms.mo"
  let assert Ok(mo_data) = simplifile.read_bits(mo_file)
  let assert Ok(lang) = language.load("fi-no-plurals", mo_data)
  let db =
    database.new()
    |> database.add_language(lang)
  let ctx = context.Context(db, "fi-no-plurals")

  should.equal(
    language.get_plural_translation(lang, "Wibble", 1),
    Error(language.LanguageHasNoPlurals),
  )

  should.equal(
    n_(ctx, "I biked one kilometre", "I biked %s kilometres", 1),
    "I biked one kilometre",
  )

  should.equal(
    n_(ctx, "I biked one kilometre", "I biked %s kilometres", 2),
    "I biked %s kilometres",
  )

  // Singular should work
  should.equal(g_(ctx, "Read more…"), "Lue lisää…")
}

fn load_languages() {
  database.new()
  |> database.add_language(load_language("fi"))
  |> database.add_language(load_language("uk"))
  |> context.Context("en")
}

fn load_language(lang_code: String) {
  let mo_file =
    "./test/locale/" <> lang_code <> "/LC_MESSAGES/" <> lang_code <> ".mo"
  let assert Ok(mo_data) = simplifile.read_bits(mo_file)
  let assert Ok(lang) = language.load(lang_code, mo_data)
  lang
}
