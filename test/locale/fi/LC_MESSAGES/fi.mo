��          D      l       �      �   G   �      �   +   �   >       ^  _   x     �  #   �                          %s person %s people Gleam is a friendly language for building type-safe systems that scale! Hello, world! Oh look, it's a duck Oh look, it's %s ducks Project-Id-Version: Kielet 1.2.3
Report-Msgid-Bugs-To: mikko@ahlroth.fi
PO-Revision-Date: 2024-05-21 22:09+0300
Last-Translator: 
Language-Team: 
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.4.4
 %s henkilö %s henkilöä Gleam on ystävällinen kieli skaalautuvien ja tyyppiturvallisten järjestelmien rakentamiseen! Morjens, maailma! Oho kato, ankka Oho kato, %s ankkaa 