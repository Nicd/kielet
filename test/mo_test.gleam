import gleam/dict
import gleeunit/should
import kielet/mo
import simplifile

pub fn header_test() {
  let mo = open()
  should.equal(mo.endianness, mo.LittleEndian)
  should.equal(mo.header.revision, mo.Revision(0, 0))
  should.equal(mo.header.string_count, 5)
}

pub fn translations_test() {
  let mo = open()
  should.equal(
    mo.translations,
    dict.from_list([
      #(
        "",
        mo.Singular(
          "",
          "Project-Id-Version: Kielet 1.2.3\nReport-Msgid-Bugs-To: mikko@ahlroth.fi\nPO-Revision-Date: 2024-05-21 22:09+0300\nLast-Translator: \nLanguage-Team: \nLanguage: fi\nMIME-Version: 1.0\nContent-Type: text/plain; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=2; plural=(n != 1);\nX-Generator: Poedit 3.4.4\n",
        ),
      ),
      #(
        "%s person",
        mo.Plural(
          "",
          dict.from_list([#(0, "%s henkilö"), #(1, "%s henkilöä")]),
        ),
      ),
      #(
        "Gleam is a friendly language for building type-safe systems that scale!",
        mo.Singular(
          "",
          "Gleam on ystävällinen kieli skaalautuvien ja tyyppiturvallisten järjestelmien rakentamiseen!",
        ),
      ),
      #("Hello, world!", mo.Singular("", "Morjens, maailma!")),
      #(
        "Oh look, it's a duck",
        mo.Plural(
          "",
          dict.from_list([#(0, "Oho kato, ankka"), #(1, "Oho kato, %s ankkaa")]),
        ),
      ),
    ]),
  )
}

pub fn metadata_test() {
  let mo = open()
  should.equal(
    mo.metadata,
    dict.from_list([
      #("", ""),
      #("Content-Transfer-Encoding", "8bit"),
      #("Content-Type", "text/plain; charset=UTF-8"),
      #("Language", "fi"),
      #("Language-Team", ""),
      #("Last-Translator", ""),
      #("MIME-Version", "1.0"),
      #("PO-Revision-Date", "2024-05-21 22:09+0300"),
      #("Plural-Forms", "nplurals=2; plural=(n != 1);"),
      #("Project-Id-Version", "Kielet 1.2.3"),
      #("Report-Msgid-Bugs-To", "mikko@ahlroth.fi"),
      #("X-Generator", "Poedit 3.4.4"),
    ]),
  )
}

pub fn big_endian_test() {
  let assert Ok(mo_data) =
    simplifile.read_bits("./test/locale/big-endian/ru.mo")
  let mo =
    mo_data
    |> mo.parse()
    |> should.be_ok()
  should.equal(mo.endianness, mo.BigEndian)
  should.equal(
    mo.translations,
    dict.from_list([
      #(
        "",
        mo.Singular(
          context: "",
          content: "Project-Id-Version: NGettext test translation file\nReport-Msgid-Bugs-To: \nPOT-Creation-Date: 2012-09-06 01:37+0200\nPO-Revision-Date: 2012-09-06 17:18+0200\nLast-Translator: \nLanguage-Team: \nMIME-Version: 1.0\nContent-Type: text/plain; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=3; plural=((((n%10)==1)&&((n%100)!=11))?(0):(((((n%10)>=2)&&((n%10)<=4))&&(((n%100)<10)||((n%100)>=20)))?(1):2));\n",
        ),
      ),
      #("test2", mo.Singular(context: "", content: "тест2")),
      #(
        "test3",
        mo.Singular(context: "", content: "тест3контекст2"),
      ),
      #(
        "{0} minute",
        mo.Plural(
          context: "",
          content: dict.from_list([
            #(0, "{0} минута"),
            #(2, "{0} минут"),
            #(1, "{0} минуты"),
          ]),
        ),
      ),
      #("test", mo.Singular(context: "", content: "тест")),
    ]),
  )
}

fn open() {
  let assert Ok(mo_data) =
    simplifile.read_bits("./test/locale/fi/LC_MESSAGES/fi.mo")

  let mo = mo.parse(mo_data)
  should.be_ok(mo)
}
