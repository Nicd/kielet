import gleeunit/should
import kielet/plurals
import kielet/plurals/evaluator

pub fn parenthesized_test() {
  let input = "nplurals=1; plural=(n==1);"
  should.be_ok(plurals.parse(input))
}

pub fn parenthesized_with_ternary_test() {
  let input = "nplurals=1; plural=(n==1 ? 1 : 0);"
  should.be_ok(plurals.parse(input))
}

pub fn no_ending_semicolon_test() {
  let input = "nplurals=1; plural=0"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 1)
}

pub fn linefeed_test() {
  let input =
    "nplurals=1;
      plural=0;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 1)
}

pub fn linefeed_with_backslash_test() {
  let input =
    "nplurals=1; \\
      plural=0;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 1)
}

pub fn one_test() {
  let input = "nplurals=1; plural=0;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 1)
  should.equal(evaluator.eval(plurals.algorithm, 1), 0)
  should.equal(evaluator.eval(plurals.algorithm, 0), 0)
  should.equal(evaluator.eval(plurals.algorithm, 8), 0)
}

pub fn two_test() {
  let input = "nplurals=2; plural=n != 1;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 2)
  should.equal(evaluator.eval(plurals.algorithm, 0), 1)
  should.equal(evaluator.eval(plurals.algorithm, 1), 0)
  should.equal(evaluator.eval(plurals.algorithm, 2), 1)
}

pub fn two_french_test() {
  let input = "nplurals=2; plural=n>1;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 2)
  should.equal(evaluator.eval(plurals.algorithm, 0), 0)
  should.equal(evaluator.eval(plurals.algorithm, 1), 0)
  should.equal(evaluator.eval(plurals.algorithm, 2), 1)
}

pub fn latvian_test() {
  let input = "nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 3)
  should.equal(evaluator.eval(plurals.algorithm, 0), 2)
  should.equal(evaluator.eval(plurals.algorithm, 1), 0)
  should.equal(evaluator.eval(plurals.algorithm, 2), 1)
  should.equal(evaluator.eval(plurals.algorithm, 111), 1)
  should.equal(evaluator.eval(plurals.algorithm, 112), 1)
  should.equal(evaluator.eval(plurals.algorithm, 31), 0)
  should.equal(evaluator.eval(plurals.algorithm, 9), 1)
}

pub fn gaeilge_test() {
  let input = "nplurals=3; plural=n==1 ? 0 : n==2 ? 1 : 2;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 3)
  should.equal(evaluator.eval(plurals.algorithm, 1), 0)
  should.equal(evaluator.eval(plurals.algorithm, 2), 1)
  should.equal(evaluator.eval(plurals.algorithm, 0), 2)
  should.equal(evaluator.eval(plurals.algorithm, 10), 2)
}

pub fn gaeilge_alternate_test() {
  let input =
    "nplurals=5; plural=n == 1 ? 0 : n == 2 ? 1 : n >= 3 && n <= 6 ? 2 : n >= 7 && n <= 10 ? 3 : 4;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 5)
  should.equal(evaluator.eval(plurals.algorithm, 1), 0)
  should.equal(evaluator.eval(plurals.algorithm, 2), 1)
  should.equal(evaluator.eval(plurals.algorithm, 4), 2)
  should.equal(evaluator.eval(plurals.algorithm, 10), 3)
  should.equal(evaluator.eval(plurals.algorithm, 133), 4)
}

pub fn romanian_test() {
  let input =
    "nplurals=3; \\
    plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 3)
  should.equal(evaluator.eval(plurals.algorithm, 1), 0)
  should.equal(evaluator.eval(plurals.algorithm, 0), 1)
  should.equal(evaluator.eval(plurals.algorithm, 119), 1)
  should.equal(evaluator.eval(plurals.algorithm, 121), 2)
  should.equal(evaluator.eval(plurals.algorithm, 19), 1)
  should.equal(evaluator.eval(plurals.algorithm, 80), 2)
}

pub fn lithuanian_test() {
  let input =
    "nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2;"
  should.be_ok(plurals.parse(input))
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 3)
  should.equal(evaluator.eval(plurals.algorithm, 81), 0)
  should.equal(evaluator.eval(plurals.algorithm, 872), 1)
  should.equal(evaluator.eval(plurals.algorithm, 112), 2)
}

pub fn ukrainian_test() {
  let input =
    "nplurals=3;
      plural=n%10==1 && n%100!=11 ? 0 :
        n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 3)
  should.equal(evaluator.eval(plurals.algorithm, 21), 0)
  should.equal(evaluator.eval(plurals.algorithm, 42), 1)
  should.equal(evaluator.eval(plurals.algorithm, 11), 2)
}

pub fn czech_test() {
  let input =
    "nplurals=3;
      plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 3)
  should.equal(evaluator.eval(plurals.algorithm, 1), 0)
  should.equal(evaluator.eval(plurals.algorithm, 3), 1)
  should.equal(evaluator.eval(plurals.algorithm, 12), 2)
}

pub fn polish_test() {
  let input =
    "nplurals=3;
      plural=n==1 ? 0 :
        n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 3)
  should.equal(evaluator.eval(plurals.algorithm, 1), 0)
  should.equal(evaluator.eval(plurals.algorithm, 102), 1)
  should.equal(evaluator.eval(plurals.algorithm, 713), 2)
}

pub fn slovenian_test() {
  let input =
    "nplurals=4;
      plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 4)
  should.equal(evaluator.eval(plurals.algorithm, 320), 3)
  should.equal(evaluator.eval(plurals.algorithm, 101), 0)
  should.equal(evaluator.eval(plurals.algorithm, 202), 1)
  should.equal(evaluator.eval(plurals.algorithm, 303), 2)
}

pub fn arabic_test() {
  let input =
    "nplurals=6;
      plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3
      : n%100>=11 ? 4 : 5;"
  let assert Ok(plurals) = plurals.parse(input)
  should.equal(plurals.total, 6)
  should.equal(evaluator.eval(plurals.algorithm, 0), 0)
  should.equal(evaluator.eval(plurals.algorithm, 1), 1)
  should.equal(evaluator.eval(plurals.algorithm, 2), 2)
  should.equal(evaluator.eval(plurals.algorithm, 505), 3)
  should.equal(evaluator.eval(plurals.algorithm, 733), 4)
  should.equal(evaluator.eval(plurals.algorithm, 101), 5)
}
