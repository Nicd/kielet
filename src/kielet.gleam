import kielet/context
import kielet/database

/// Translate the given singular message.
///
/// If there is a failure to translate the message, the given message is
/// returned as-is. Causes for such a failure are:
///
/// - if no translations have been loaded for the language,
/// - if there is no translation for this message for the language, or
/// - if the translation for this message is plural.
///
/// Example:
///
/// ```gleam
/// // Imported with kielet.{gettext as g_}
/// io.println(
///   g_(ctx, "Sleep tight in a new light through another warning call")
/// )
/// ```
pub fn gettext(context: context.Context, msgid: String) -> String {
  database.translate_singular(context.database, msgid, context.language)
}

/// Translate the given plural message. `n` is the amount of countable items
/// in the message. For example for the English language, from `"%s bunny"` and
/// `"%s bunnies"`, the latter would be returned when `n` is anything except 1.
/// 
/// Note that this function does no replacing of any placeholder. It is only
/// convention to use `%s` in place of the amount in the message, and it will
/// not be altered by this function. Replacing of the amount is left to the
/// user.
///
/// If there is a failure to translate the message, the given message is
/// returned, in singular or plural, using the English pluralisation rules.
/// Causes for such a failure are:
///
/// - if no translations have been loaded for the language,
/// - if there is no translation for this message for the language,
/// - if the translation for this message is singular,
/// - if the plural form algorithm returned a form that does not exist in the
///   translation, or
/// - if the translation file did not have a `Plural-Forms` header.
///
/// Example:
/// 
/// ```gleam
/// // Imported with kielet.{ngettext as n_}
///
/// let n = 100
///
/// io.println(
///   string.replace(
///     n_(
///       ctx,
///       "That's better than a rabbit",
///       "That's better than %s rabbits",
///       n
///     ),
///     "%s",
///     int.to_string(n)
///   )
/// )
/// ```
pub fn ngettext(
  context: context.Context,
  singular: String,
  plural: String,
  n: Int,
) -> String {
  database.translate_plural(
    context.database,
    singular,
    plural,
    n,
    context.language,
  )
}
