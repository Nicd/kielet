//// A language contains translations and plural form information, and can be
//// used to translate messages.

import gleam/dict
import gleam/option
import gleam/result
import kielet/mo
import kielet/plurals

/// Error returned when an MO file is loaded.
pub type LanguageLoadError {
  /// The MO file could not be parsed.
  MoParseError(err: mo.ParseError)

  /// The plural forms in the MO file were invalid or missing.
  PluralFormsLoadError(err: plurals.LoadError)
}

/// Error when trying to translate a message.
pub type TranslateError {
  /// Tried to translate a plural message, but the translation is singular.
  MsgIsSingular(String)

  /// Tried to translate a singular message, but the translation is plural.
  MsgIsPlural(String)

  /// There was no translation for the message.
  MsgNotFound(String)

  /// The plural algorithm returned a form that is out of bounds for the amount
  /// of plural forms.
  PluralOutOfBounds(requested: Int, but_max_is: Int)

  /// The translation file had no plural forms or the plural forms header was
  /// missing.
  LanguageHasNoPlurals
}

pub opaque type Language {
  Language(
    code: String,
    translations: mo.Translations,
    /// A translation file may not have any plurals and no plural-forms header.
    /// In that case this value is `None` and plural translations will fail.
    plurals: option.Option(plurals.Plurals),
  )
}

/// Load a language from the given MO file.
pub fn load(code: String, mo_file: BitArray) {
  use mo <- result.try(
    mo.parse(mo_file)
    |> result.map_error(MoParseError),
  )

  case plurals.load_from_mo(mo) {
    Ok(p) -> Ok(Language(code, mo.translations, option.Some(p)))
    Error(plurals.NoPluralFormsHeader) ->
      Ok(Language(code, mo.translations, option.None))
    Error(err) -> Error(PluralFormsLoadError(err))
  }
}

/// Get the language's language code.
pub fn get_code(lang: Language) {
  lang.code
}

/// Translate a singular message.
pub fn get_singular_translation(lang: Language, msgid: String) {
  case dict.get(lang.translations, msgid) {
    Ok(mostring) ->
      case mostring {
        mo.Singular(content: c, ..) -> Ok(c)
        _ -> Error(MsgIsPlural(msgid))
      }
    _ -> Error(MsgNotFound(msgid))
  }
}

/// Translate a plural message.
pub fn get_plural_translation(lang: Language, msgid: String, n: Int) {
  case lang.plurals, dict.get(lang.translations, msgid) {
    option.None, _ -> Error(LanguageHasNoPlurals)
    option.Some(p), Ok(mostring) ->
      case mostring {
        mo.Plural(content: c, ..) -> {
          let index = plurals.evaluate(p, n)
          case dict.get(c, index) {
            Ok(msg) -> Ok(msg)
            Error(_) ->
              Error(PluralOutOfBounds(
                requested: index,
                but_max_is: dict.size(c) - 1,
              ))
          }
        }
        _ -> Error(MsgIsSingular(msgid))
      }
    option.Some(_), _ -> Error(MsgNotFound(msgid))
  }
}
