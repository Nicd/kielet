//// The database contains all the loaded languages.

import gleam/dict.{type Dict}
import kielet/language.{type Language}

pub opaque type Database {
  Database(languages: Dict(String, Language))
}

/// Create a new empty database.
pub fn new() {
  Database(languages: dict.new())
}

/// Add a language to the database.
pub fn add_language(db: Database, lang: Language) {
  Database(languages: dict.insert(db.languages, language.get_code(lang), lang))
}

/// Translate a singular message using the database.
///
/// If the language is not found or does not have a translation for the message,
/// the message is returned as-is.
pub fn translate_singular(db: Database, msgid: String, language_code: String) {
  case dict.get(db.languages, language_code) {
    Ok(lang) ->
      case language.get_singular_translation(lang, msgid) {
        Ok(translation) -> translation
        Error(_) -> msgid
      }
    Error(_) -> msgid
  }
}

/// Translate a plural message using the database.
///
/// If the language is not found, does not have a translation for the message,
/// does not have the correct plural for the given `n`, or does not have a
/// plural forms header at all, the plural message given as the argument is
/// returned instead.
pub fn translate_plural(
  db: Database,
  msgid: String,
  plural: String,
  n: Int,
  language_code: String,
) {
  case dict.get(db.languages, language_code) {
    Ok(lang) ->
      case language.get_plural_translation(lang, msgid, n) {
        Ok(translation) -> translation
        Error(_) -> default_pick_plural(msgid, plural, n)
      }
    Error(_) -> default_pick_plural(msgid, plural, n)
  }
}

fn default_pick_plural(msgid: String, plural: String, n: Int) {
  case n {
    1 -> msgid
    _ -> plural
  }
}
