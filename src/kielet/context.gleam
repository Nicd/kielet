import kielet/database

/// Translation context, containing both the database and the active language
/// in one type for convenience.
pub type Context {
  Context(database: database.Database, language: String)
}
