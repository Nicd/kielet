//// The tokenizer converts the plural forms syntax into a list of tokens for
//// later parsing.

// Translated from Expo tokenizer

import gleam/int
import gleam/list
import gleam/option
import gleam/string
import kielet/plurals/syntax_error.{SyntaxError}
import nibble/lexer

pub type Token {
  N
  NPlurals
  Plural
  Equals
  NotEquals
  GreaterThanOrEquals
  LowerThanOrEquals
  GreaterThan
  LowerThan
  Assignment
  Ternary
  TernaryElse
  Remainder
  Or
  And
  Semicolon
  LParen
  RParen
  End
  Int(value: Int)
}

/// Tokenize the given plural forms syntax.
///
/// Whitespace is ignored and backslashes at the end of lines are removed.
pub fn tokenize(str: String) {
  do_tokenize(string.to_graphemes(str), [], 1, 1)
}

fn do_tokenize(
  str: List(String),
  acc: List(lexer.Token(Token)),
  line: Int,
  col: Int,
) {
  case str {
    [] -> Ok(list.reverse([to_nibble(End, "", line, col), ..acc]))
    ["n", "p", "l", "u", "r", "a", "l", "s", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(NPlurals, "nplurals", line, col), ..acc],
        line,
        col + 8,
      )
    ["p", "l", "u", "r", "a", "l", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(Plural, "plural", line, col), ..acc],
        line,
        col + 6,
      )
    ["n", ..rest] ->
      do_tokenize(rest, [to_nibble(N, "n", line, col), ..acc], line, col + 1)
    ["\\", "\n", ..rest] -> do_tokenize(rest, acc, line + 1, 1)
    ["\n", ..rest] -> do_tokenize(rest, acc, line + 1, 1)
    [" ", ..rest] -> do_tokenize(rest, acc, line, col + 1)
    ["=", "=", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(Equals, "==", line, col), ..acc],
        line,
        col + 2,
      )
    ["!", "=", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(NotEquals, "!=", line, col), ..acc],
        line,
        col + 2,
      )
    [">", "=", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(GreaterThanOrEquals, ">=", line, col), ..acc],
        line,
        col + 2,
      )
    ["<", "=", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(LowerThanOrEquals, "<=", line, col), ..acc],
        line,
        col + 2,
      )
    [">", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(GreaterThan, ">", line, col), ..acc],
        line,
        col + 1,
      )
    ["<", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(LowerThan, "<", line, col), ..acc],
        line,
        col + 1,
      )
    ["=", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(Assignment, "=", line, col), ..acc],
        line,
        col + 1,
      )
    ["?", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(Ternary, "?", line, col), ..acc],
        line,
        col + 1,
      )
    [":", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(TernaryElse, ":", line, col), ..acc],
        line,
        col + 1,
      )
    ["%", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(Remainder, "%", line, col), ..acc],
        line,
        col + 1,
      )
    ["|", "|", ..rest] ->
      do_tokenize(rest, [to_nibble(Or, "||", line, col), ..acc], line, col + 2)
    ["&", "&", ..rest] ->
      do_tokenize(rest, [to_nibble(And, "&&", line, col), ..acc], line, col + 2)
    [";", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(Semicolon, ";", line, col), ..acc],
        line,
        col + 1,
      )
    [")", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(RParen, ")", line, col), ..acc],
        line,
        col + 1,
      )
    ["(", ..rest] ->
      do_tokenize(
        rest,
        [to_nibble(LParen, "(", line, col), ..acc],
        line,
        col + 1,
      )
    [digit, ..rest] if digit == "0"
      || digit == "1"
      || digit == "2"
      || digit == "3"
      || digit == "4"
      || digit == "5"
      || digit == "6"
      || digit == "7"
      || digit == "8"
      || digit == "9" -> read_digits(rest, acc, line, col + 1, digit)
    [grapheme, ..] ->
      Error(SyntaxError(
        reason: "Unexpected grapheme " <> grapheme,
        line: line,
        column: option.Some(col),
      ))
  }
}

fn read_digits(
  str: List(String),
  acc: List(lexer.Token(Token)),
  line: Int,
  col: Int,
  digit_acc: String,
) {
  case str {
    [digit, ..rest]
      if digit == "0"
      || digit == "1"
      || digit == "2"
      || digit == "3"
      || digit == "4"
      || digit == "5"
      || digit == "6"
      || digit == "7"
      || digit == "8"
      || digit == "9"
    -> read_digits(rest, acc, line, col + 1, digit_acc <> digit)
    other ->
      case int.parse(digit_acc) {
        Ok(int) ->
          do_tokenize(
            other,
            [to_nibble(Int(int), digit_acc, line, col), ..acc],
            line,
            col + string.length(digit_acc),
          )
        Error(_) ->
          Error(SyntaxError(
            reason: "Unparseable integer " <> digit_acc,
            line: line,
            column: option.Some(col),
          ))
      }
  }
}

fn to_nibble(token: Token, lexeme: String, line: Int, col: Int) {
  lexer.Token(
    span: lexer.Span(
      row_start: line,
      row_end: line,
      col_start: col,
      col_end: col + string.length(lexeme),
    ),
    lexeme: lexeme,
    value: token,
  )
}
