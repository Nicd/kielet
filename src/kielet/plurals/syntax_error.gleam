import gleam/option

// Translated from Expo syntax error type

/// Error returned when the syntax in a plural forms string is invalid.
pub type SyntaxError {
  SyntaxError(line: Int, column: option.Option(Int), reason: String)
}
