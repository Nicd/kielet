//// Evaluation engine for the syntax tree

// Translated from Expo evaluator

import kielet/plurals/ast

/// Evaluate the given syntax tree with the input, returning an integer value
/// representing the index of the plural form to choose.
pub fn eval(ast: ast.Ast, input: Int) {
  case ast {
    ast.N -> input
    ast.Integer(i) -> i
    ast.If(condition, truthy, falsy) -> {
      let ast = case eval(condition, input) {
        1 -> truthy
        _ -> falsy
      }
      eval(ast, input)
    }
    ast.Paren(content) -> eval(content, input)
    ast.BinaryOperation(operator, lvalue, rvalue) -> {
      let lvalue = eval(lvalue, input)
      let rvalue = eval(rvalue, input)
      case operator {
        ast.Equal -> bool_to_int(lvalue == rvalue)
        ast.NotEqual -> bool_to_int(lvalue != rvalue)
        ast.GreaterThan -> bool_to_int(lvalue > rvalue)
        ast.GreaterThanOrEqual -> bool_to_int(lvalue >= rvalue)
        ast.LowerThan -> bool_to_int(lvalue < rvalue)
        ast.LowerThanOrEqual -> bool_to_int(lvalue <= rvalue)
        ast.Remainder -> lvalue % rvalue
        ast.And -> bool_to_int(lvalue == 1 && rvalue == 1)
        ast.Or -> bool_to_int(lvalue == 1 || rvalue == 1)
      }
    }
  }
}

fn bool_to_int(bool: Bool) {
  case bool {
    True -> 1
    False -> 0
  }
}
