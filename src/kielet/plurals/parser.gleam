//// Parser for the plural form token stream.
////
//// The trailing semicolon in the format is optional.

import gleam/option
import kielet/plurals/ast
import kielet/plurals/tokenizer
import nibble
import nibble/lexer
import nibble/pratt

/// Parse the given list of tokens into an abstract syntax tree.
pub fn parse(input: List(lexer.Token(tokenizer.Token))) {
  nibble.run(input, main_parser())
}

fn main_parser() {
  use _ <- nibble.do(nibble.token(tokenizer.NPlurals))
  use _ <- nibble.do(nibble.token(tokenizer.Assignment))
  use nplurals <- nibble.do(int_parser())
  use _ <- nibble.do(nibble.token(tokenizer.Semicolon))
  use _ <- nibble.do(nibble.token(tokenizer.Plural))
  use _ <- nibble.do(nibble.token(tokenizer.Assignment))
  use ast <- nibble.do(plurals_parser())
  use _ <- nibble.do(nibble.optional(nibble.token(tokenizer.Semicolon)))
  use _ <- nibble.do(nibble.token(tokenizer.End))
  use _ <- nibble.do(nibble.eof())

  let assert ast.Integer(nplurals) = nplurals

  nibble.return(#(nplurals, ast))
}

fn plurals_parser() {
  use maybe_cond <- nibble.do(expr_parser())

  nibble.one_of([rest_of_ternary_parser(maybe_cond), nibble.return(maybe_cond)])
}

fn expr_parser() {
  pratt.expression(
    one_of: [
      fn(_) { int_parser() },
      fn(_) { n_parser() },
      fn(_) { paren_parser() },
    ],
    and_then: [
      pratt.infix_left(200, nibble.token(tokenizer.And), fn(l, r) {
        ast.BinaryOperation(ast.And, l, r)
      }),
      pratt.infix_left(200, nibble.token(tokenizer.Or), fn(l, r) {
        ast.BinaryOperation(ast.Or, l, r)
      }),
      pratt.infix_left(300, nibble.token(tokenizer.Equals), fn(l, r) {
        ast.BinaryOperation(ast.Equal, l, r)
      }),
      pratt.infix_left(300, nibble.token(tokenizer.NotEquals), fn(l, r) {
        ast.BinaryOperation(ast.NotEqual, l, r)
      }),
      pratt.infix_left(300, nibble.token(tokenizer.GreaterThan), fn(l, r) {
        ast.BinaryOperation(ast.GreaterThan, l, r)
      }),
      pratt.infix_left(300, nibble.token(tokenizer.LowerThan), fn(l, r) {
        ast.BinaryOperation(ast.LowerThan, l, r)
      }),
      pratt.infix_left(
        300,
        nibble.token(tokenizer.GreaterThanOrEquals),
        fn(l, r) { ast.BinaryOperation(ast.GreaterThanOrEqual, l, r) },
      ),
      pratt.infix_left(300, nibble.token(tokenizer.LowerThanOrEquals), fn(l, r) {
        ast.BinaryOperation(ast.LowerThanOrEqual, l, r)
      }),
      pratt.infix_right(400, nibble.token(tokenizer.Remainder), fn(l, r) {
        ast.BinaryOperation(ast.Remainder, l, r)
      }),
    ],
    dropping: nibble.return(Nil),
  )
}

fn paren_parser() {
  use _ <- nibble.do(lparen_parser())
  use expr <- nibble.do(plurals_parser())
  use _ <- nibble.do(rparen_parser())

  nibble.return(ast.Paren(expr))
}

fn rest_of_ternary_parser(cond: ast.Ast) {
  use _ <- nibble.do(nibble.token(tokenizer.Ternary))
  use if_true <- nibble.do(plurals_parser())
  use _ <- nibble.do(nibble.token(tokenizer.TernaryElse))
  use if_false <- nibble.do(plurals_parser())

  nibble.return(ast.If(cond, if_true, if_false))
}

fn int_parser() {
  use tok <- nibble.take_map("An integer")

  case tok {
    tokenizer.Int(i) -> option.Some(ast.Integer(i))
    _ -> option.None
  }
}

fn n_parser() {
  use tok <- nibble.take_map("n")

  case tok {
    tokenizer.N -> option.Some(ast.N)
    _ -> option.None
  }
}

fn lparen_parser() {
  use tok <- nibble.take_map("Left parenthesis")

  case tok {
    tokenizer.LParen -> option.Some(Nil)
    _ -> option.None
  }
}

fn rparen_parser() {
  use tok <- nibble.take_map("Right parenthesis")

  case tok {
    tokenizer.RParen -> option.Some(Nil)
    _ -> option.None
  }
}
