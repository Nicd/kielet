//// Plural forms abstract syntax tree

/// A binary operation (operation with two arguments)
pub type BinOp {
  Equal
  NotEqual
  GreaterThan
  GreaterThanOrEqual
  LowerThan
  LowerThanOrEqual
  Remainder
  And
  Or
}

pub type Ast {
  /// Represents the input value
  N
  /// Integer literal
  Integer(Int)
  BinaryOperation(operator: BinOp, lvalue: Ast, rvalue: Ast)
  If(condition: Ast, truthy: Ast, falsy: Ast)
  /// Parenthesised expression
  Paren(Ast)
}
