//// Utilities for plural forms.

import gleam/dict
import gleam/result
import kielet/mo
import kielet/plurals/ast
import kielet/plurals/evaluator
import kielet/plurals/parser
import kielet/plurals/syntax_error
import kielet/plurals/tokenizer
import nibble

const plural_forms_header = "Plural-Forms"

/// Error returned when tokenizing or parsing the plural forms.
pub type ParseError {
  TokenizerError(err: syntax_error.SyntaxError)
  ParserError(err: List(nibble.DeadEnd(tokenizer.Token, Nil)))
}

/// Error returned when loading the plural forms from an MO file.
pub type LoadError {
  ParsingFailed(err: ParseError)
  NoPluralFormsHeader
}

pub type Plurals {
  Plurals(total: Int, algorithm: ast.Ast)
}

/// Load plural forms from a parsed MO file.
pub fn load_from_mo(mo: mo.Mo) {
  use plural_header <- result.try(
    dict.get(mo.metadata, plural_forms_header)
    |> result.replace_error(NoPluralFormsHeader),
  )
  parse(plural_header)
  |> result.map_error(ParsingFailed)
}

/// Tokenize and parse given plural forms string.
pub fn parse(input: String) {
  use tokens <- result.try(
    tokenizer.tokenize(input)
    |> result.map_error(TokenizerError),
  )
  use #(total, ast) <- result.try(
    parser.parse(tokens)
    |> result.map_error(ParserError),
  )
  Ok(Plurals(total: total, algorithm: ast))
}

/// Evaluate given plurals, returning the index of the plural form to choose.
pub fn evaluate(plurals: Plurals, n: Int) {
  evaluator.eval(plurals.algorithm, n)
}
