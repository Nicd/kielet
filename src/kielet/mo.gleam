//// An MO file parser.
////
//// Both little and big endian files are supported.
////
//// All strings must be UTF-8.

import gleam/bit_array
import gleam/bool
import gleam/dict.{type Dict}
import gleam/int
import gleam/list
import gleam/result
import gleam/string

const max_supported_major = 0

const eot = ""

const nul = "\u{0}"

/// A translation in the file. The context is a free-form text that can be
/// added as extra context for the translators.
pub type MoString {
  Singular(context: String, content: String)
  Plural(context: String, content: Dict(Int, String))
}

/// Dictionary of translations keyed by the original msgid.
pub type Translations =
  Dict(String, MoString)

pub type Endianness {
  BigEndian
  LittleEndian
}

pub type ParseError {
  MagicNumberNotFound
  MalformedHeader
  UnknownRevision(Revision)

  /// An offset was given that either pointed directly out of bounds or the data
  /// it pointed to would have been too big for the file.
  OffsetPastEnd(Int)
  MalformedOffsetTableEntry(BitArray)
  StringNotUTF8(BitArray)

  /// Metadata is contained as a "translation" for the msgid "" (empty string).
  /// If this translation is missing, this error is returned.
  MetaItemMissing

  /// The metadata item is a plural.
  MetaItemIsNotSingular
}

pub type Revision {
  Revision(major: Int, minor: Int)
}

pub type Header {
  Header(
    revision: Revision,
    /// Amount of strings in file.
    string_count: Int,
    /// Offset to table of strings in original language.
    og_table_offset: Int,
    /// Offset to translations table.
    trans_table_offset: Int,
    /// Hash table size (not used by this parser).
    ht_size: Int,
    /// Hash table offset.
    ht_offset: Int,
  )
}

/// An MO file can contain metadata that looks quite like HTTP headers. The most
/// important data here are the plural forms with the key `Plural-Forms`.
pub type MetaData =
  Dict(String, String)

pub type Mo {
  Mo(
    endianness: Endianness,
    header: Header,
    translations: Translations,
    metadata: MetaData,
  )
}

type EndiannessHandler {
  EndiannessHandler(
    int_8: fn(BitArray) -> Result(Int, Nil),
    int_32: fn(BitArray) -> Result(Int, Nil),
  )
}

/// Parse given MO file data.
pub fn parse(mo: BitArray) {
  use #(endianness, rest) <- result.try(parse_magic(mo))

  let endianness_handler = case endianness {
    LittleEndian -> EndiannessHandler(int_8: le_int_8, int_32: le_int_32)
    BigEndian -> EndiannessHandler(int_8: be_int_8, int_32: be_int_32)
  }

  use header <- result.try(parse_header(endianness_handler, rest))
  use <- bool.guard(
    header.revision.major > max_supported_major,
    Error(UnknownRevision(header.revision)),
  )
  use <- bool.guard(
    header.string_count == 0,
    Ok(Mo(
      endianness: endianness,
      header: header,
      translations: dict.new(),
      metadata: dict.new(),
    )),
  )

  let total_size = bit_array.byte_size(mo)

  use <- bool.guard(
    header.og_table_offset >= total_size,
    Error(OffsetPastEnd(header.og_table_offset)),
  )
  use <- bool.guard(
    header.trans_table_offset >= total_size,
    Error(OffsetPastEnd(header.trans_table_offset)),
  )
  use <- bool.guard(
    header.ht_offset >= total_size,
    Error(OffsetPastEnd(header.ht_offset)),
  )

  use translations <- result.try(parse_translations(
    endianness_handler,
    header,
    mo,
  ))
  use metadata <- result.try(parse_metadata(translations))

  Ok(Mo(
    endianness: endianness,
    header: header,
    translations: translations,
    metadata: metadata,
  ))
}

fn parse_magic(body: BitArray) {
  case body {
    <<0xde120495:size(32), rest:bytes>> -> Ok(#(LittleEndian, rest))
    <<0x950412de:size(32), rest:bytes>> -> Ok(#(BigEndian, rest))
    _ -> Error(MagicNumberNotFound)
  }
}

fn parse_header(eh: EndiannessHandler, body: BitArray) {
  case body {
    <<
      major_bytes:bytes-size(2),
      minor_bytes:bytes-size(2),
      string_count_bytes:bytes-size(4),
      og_table_offset_bytes:bytes-size(4),
      trans_table_offset_bytes:bytes-size(4),
      ht_size_bytes:bytes-size(4),
      ht_offset_bytes:bytes-size(4),
      _rest:bytes,
    >> -> {
      let assert Ok(major) = eh.int_8(major_bytes)
      let assert Ok(minor) = eh.int_8(minor_bytes)
      let assert Ok(string_count) = eh.int_32(string_count_bytes)
      let assert Ok(og_table_offset) = eh.int_32(og_table_offset_bytes)
      let assert Ok(trans_table_offset) = eh.int_32(trans_table_offset_bytes)
      let assert Ok(ht_size) = eh.int_32(ht_size_bytes)
      let assert Ok(ht_offset) = eh.int_32(ht_offset_bytes)
      Ok(Header(
        Revision(major, minor),
        string_count,
        og_table_offset,
        trans_table_offset,
        ht_size,
        ht_offset,
      ))
    }
    _ -> Error(MalformedHeader)
  }
}

fn parse_translations(eh: EndiannessHandler, header: Header, mo: BitArray) {
  let strings = list.range(0, header.string_count - 1)
  use translations <- result.try(
    list.try_fold(strings, dict.new(), fn(translations, i) {
      let new_offset = i * 8
      let og_offset = header.og_table_offset + new_offset
      let trans_offset = header.trans_table_offset + new_offset
      use #(og, translation) <- result.try(parse_translation(
        eh,
        mo,
        og_offset,
        trans_offset,
      ))

      let key = case og {
        Singular(content: c, ..) -> c
        Plural(content: c, ..) ->
          case dict.get(c, 0) {
            Ok(c) -> c
            Error(_) -> panic as "Got plural form with zero entries"
          }
      }

      Ok(dict.insert(translations, key, translation))
    }),
  )

  Ok(translations)
}

fn parse_translation(
  eh: EndiannessHandler,
  mo: BitArray,
  og_offset: Int,
  trans_offset: Int,
) {
  use #(og_str_length, og_str_offset) <- result.try(parse_offset_table_entry(
    eh,
    mo,
    og_offset,
  ))
  use #(trans_str_length, trans_str_offset) <- result.try(
    parse_offset_table_entry(eh, mo, trans_offset),
  )

  use og_string <- result.try(parse_mo_string(mo, og_str_length, og_str_offset))
  use trans_string <- result.try(parse_mo_string(
    mo,
    trans_str_length,
    trans_str_offset,
  ))

  Ok(#(og_string, trans_string))
}

fn parse_offset_table_entry(eh: EndiannessHandler, mo: BitArray, offset: Int) {
  use data <- result.try(
    bit_array.slice(mo, offset, 8)
    |> result.replace_error(OffsetPastEnd(offset)),
  )
  case data {
    <<target_length:bytes-size(4), target_offset:bytes-size(4)>> -> {
      let assert Ok(target_length) = eh.int_32(target_length)
      let assert Ok(target_offset) = eh.int_32(target_offset)
      Ok(#(target_length, target_offset))
    }
    _ -> Error(MalformedOffsetTableEntry(data))
  }
}

fn parse_mo_string(mo: BitArray, length: Int, offset: Int) {
  use data <- result.try(
    bit_array.slice(mo, offset, length)
    |> result.replace_error(OffsetPastEnd(offset)),
  )

  use str <- result.try(
    bit_array.to_string(data)
    |> result.replace_error(StringNotUTF8(data)),
  )

  let #(context, str) = case string.split_once(str, eot) {
    Ok(#(c, s)) -> #(c, s)
    Error(_) -> #("", str)
  }

  case string.split(str, nul) {
    [_] -> Ok(Singular(context: context, content: str))
    plurals ->
      Ok(Plural(
        context: context,
        content: plurals
          |> list.index_map(fn(msg, i) { #(i, msg) })
          |> dict.from_list(),
      ))
  }
}

fn parse_metadata(translations) {
  use meta <- result.try(
    dict.get(translations, "")
    |> result.replace_error(MetaItemMissing),
  )
  case meta {
    Plural(..) -> Error(MetaItemIsNotSingular)
    Singular(content: content, ..) -> {
      let metadata =
        content
        |> string.split("\n")
        |> list.map(fn(line) { string.split(line, ":") })
        |> list.map(fn(item) {
          case item {
            [] -> #("", "")
            [key] -> #(string.trim(key), "")
            [key, ..rest] -> #(
              string.trim(key),
              string.trim(string.join(rest, ":")),
            )
          }
        })
        |> dict.from_list()

      Ok(metadata)
    }
  }
}

fn le_int_8(int8: BitArray) {
  case int8 {
    <<l:size(8), h:size(8)>> -> Ok(reconstruct_ui8(h, l))
    _ -> Error(Nil)
  }
}

fn be_int_8(int8: BitArray) {
  case int8 {
    <<h:size(8), l:size(8)>> -> Ok(reconstruct_ui8(h, l))
    _ -> Error(Nil)
  }
}

fn le_int_32(int32: BitArray) {
  case int32 {
    <<ll:size(8), lh:size(8), hl:size(8), hh:size(8)>> ->
      Ok(reconstruct_ui32(hh, hl, lh, ll))
    _ -> Error(Nil)
  }
}

fn be_int_32(int32: BitArray) {
  case int32 {
    <<hh:size(8), hl:size(8), lh:size(8), ll:size(8)>> ->
      Ok(reconstruct_ui32(hh, hl, lh, ll))
    _ -> Error(Nil)
  }
}

fn reconstruct_ui8(h: Int, l: Int) {
  int.bitwise_shift_left(h, 8) + l
}

fn reconstruct_ui32(hh: Int, hl: Int, lh: Int, ll: Int) {
  int.bitwise_shift_left(hh, 8 * 3)
  + int.bitwise_shift_left(hl, 8 * 2)
  + int.bitwise_shift_left(lh, 8)
  + ll
}
